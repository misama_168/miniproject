const express = require('express');
const LoginController = require('../controller/login.js');
const router = express.Router();

router.post('/login',LoginController.Login);
router.post('/register',LoginController.Register);

module.exports = router;
