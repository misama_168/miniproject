const express = require('express');
const UserController = require('../controller/users.js');

const router = express.Router();

router.get('/',UserController.getAllUsers);

router.get('/:id',UserController.getUserbyid);

router.patch('/:id',UserController.updateUser);

router.delete('/:id',UserController.DeleteUsers);

module.exports = router;