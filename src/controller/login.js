const userModels = require('../models/users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const logerr =   {code: 500, msg : 'Request timeout !' }

const Login = async (req,res) => {
    const {body} = req;
    try {
       const [user] = await userModels.LoginUser(body);
       const match = await bcrypt.compare(req.body.password, user[0].password);
       if(!match) return res.status(400).json({msg: "Wrong Password"});
       const userId = user[0].id;
       const name = user[0].name;
       const email = user[0].email;
       const params = {id : userId , name : name , email : email}
        const accessToken = jwt.sign({ id: userId }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: process.env.JWT_EXPIRE,
        });

       await userModels.UpdateToken(accessToken, userId);
       return res.cookie({"token":accessToken}).json({ 
        code : 200,
        msg: 'Login success !',
        data : params,
        token : accessToken
      })

    } catch (error) {
        
        res.json(logerr)
    }
}


const Register = async(req, res) => {
    const { name, email, password,age,hobi,address,phone } = req.body;

    if(!name || !email || !password || !age || !address || !phone){
        return res.status(400).json({
            msg : 'data belum lengkap',
        })
    }

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    try {
        await userModels.CreateUserBaru({
            name: name,
            email: email,
            age: age,
            address: address,
            hobi: hobi,
            phone:phone,
            password: hashPassword
        });
        res.json({msg: "Register Berhasil"});
    } catch (error) {
        res.json({msg: error});
    }
}

module.exports = {
    Login,
    Register
}


