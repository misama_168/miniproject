const userModels = require('../models/users');

const logerr =   {code: 500, msg : 'Request timeout !' }

const getAllUsers = async (req,res) => {
  
    try { 
        const [result, fields] = await userModels.getUser();
        const data = []
        result.forEach(function(object){
          data.push({
            id: object.id,
            name: object.name,
            email: object.email,
            age: object.age,
            address: object.address,
            hobi : object.hobi
          });
        });
        res.json({
            code : 200,
            msg: 'Show all users success !',
            data : data
        })

    } catch (error) {
        
       res.json(logerr)
        
    }

}


const getUserbyid = async (req,res)=>{
    const {id} = req.params;
    try {
        const [result , fields] =  await userModels.getUserByid(id);
        const data = []
        result.forEach(function(object){
          data.push({
            id: object.id,
            name: object.name,
            age: object.age,
            address: object.address,
            hobi : object.hobi
          });
        });
        res.json({
            code : 200,
            msg: 'Show users success !',
            data : data
        })
    } catch (error) {
        
        res.json(logerr)
      
    }
}


const updateUser = async (req,res) =>{
    const {id} = req.params;
    const {body} = req;
    if(!body.name || !body.email || !body.age || !body.address || !body.phone){
        return res.status(400).json({
            msg : 'data belum lengkap',
        })
    }

    try {
        
    await userModels.UpdateUserx(body, id);
    res.json({
        code : 200,
        msg:'Update Users success !',
        data: body
    })
  } catch (error) {
    
    res.json({
        m : error
    })

  }
   
}

const DeleteUsers = async (req,res)=>{
    const {id} = req.params;
    try {
        await userModels.DeleteUserx(id);
        res.json({
            code : 200,
            msg:'Delete Users success !',
        })
    } catch (error) {
        
        res.json(logerr)
      
    }
} 

module.exports = {
    getAllUsers,
    updateUser,
    DeleteUsers,
    getUserbyid
}