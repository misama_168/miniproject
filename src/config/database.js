// const mysql = require('mysql');
const mysql = require('mysql2');

//set koneksi DB
const db = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
});

db.getConnection(function(err) {
    if (err) throw err;
    console.log("DB Connected!");
});


module.exports = db.promise();