require('dotenv').config()
const express = require('express');
const app = express();
const cors = require("cors");
const cookieParser = require("cookie-parser");
const UserRoute = require('./routes/users.js');
const RouteLogin = require('./routes/login.js');
const verifyToken = require('./middleware/verifytoken.js');

app.use(cors({ credentials:true, origin:'http://localhost:4000' }));
app.use(cookieParser());

const port = process.env.PORT;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/", RouteLogin);
app.use("/api/users",verifyToken, UserRoute);

app.listen(port, ()=>{
    console.log('running on port ' + port)
})
