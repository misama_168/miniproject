const DB = require('../config/database.js');

const getUser = () =>{

    const query = 'select * from users';
    return DB.execute(query);
}

const getUserByid = (id) =>{

    const query = `select * from users where id = '${id}' `;
    return DB.execute(query);
}

const CreateUserBaru = (body) =>{

    const query = `insert into users (name,email,hobi,age,address,password,phone) values('${body.name}','${body.email}','${body.hobi}','${body.age}','${body.address}','${body.password}','${body.phone}')`;
    return DB.execute(query);
}

const UpdateUserx = (body,id) =>{

    const query = `update users set name='${body.name}',email ='${body.email}', hobi='${body.hobi}',
                    age='${body.age}',address='${body.address}'
                    where id='${id}'
                   `;
    return DB.execute(query);
}


const DeleteUserx = (id) => {
    const query = `delete from users where id ='${id}'`;
    return DB.execute(query);
}


const LoginUser =  (body) =>{
    const query = `select * from users where email like '${body.email}'`;
    return DB.execute(query);
}

const UpdateToken = (token,id) => {
    const query = `update users set token='${token}' where id ='${id}'`;
    return DB.execute(query);
}

module.exports = {
    getUser,
    CreateUserBaru,
    UpdateUserx,
    DeleteUserx,
    getUserByid,
    LoginUser,
    UpdateToken
}